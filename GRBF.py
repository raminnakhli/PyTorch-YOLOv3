import numpy as np
import torch
from torch import nn
from torch.nn.modules.loss import _Loss


class GRBFLayer(nn.Module):
    """
    GRBF layer for classification
    """

    def __init__(self, num_in_feature, num_shared_feature, num_out_feature, num_classes, p):
        super(GRBFLayer, self).__init__()
        self.num_classes = num_classes
        self.num_out_feature = num_out_feature
        self.shared_fc = nn.Identity()
        if num_shared_feature != 0:
            self.shared_fc = nn.Linear(num_in_feature, num_shared_feature, bias=False)
            self.add_module('Linear_Shared', self.shared_fc)
        self.class_fc = nn.Linear(num_shared_feature, num_out_feature * num_classes)
        self.add_module('Linear_Specific', self.class_fc)
        if p == 1:
            self.distance_function = lambda x: torch.sum(torch.abs(x), dim=-1)
        elif p == 2:
            self.distance_function = lambda x: torch.sum(x.pow(2), dim=-1)
        else:
            raise ValueError("p must be 1 or 2")
        # self.distance_function = lambda x: torch.norm(x, p=p, dim=-1).pow(p)

    def forward(self, x):
        x = self.shared_fc(x)
        dim_len = len(x.size())
        x = self.class_fc(x).view(*x.size()[:dim_len - 1], self.num_classes, self.num_out_feature)
        x = self.distance_function(x).squeeze(dim=-1)
        p = torch.exp(-1 * x)
        d = -x
        return p, d


class SoftML(_Loss):
    """
    SoftML loss function for GRBF model

    Shape:
        - Input: :math:`(N, C)` where `C = number of classes`, or
          :math:`(N, C, d_1, d_2, ..., d_K)` with :math:`K \geq 1`
          in the case of `K`-dimensional loss.
        - Target: :math:`(N)` where each value is :math:`0 \leq \text{targets}[i] \leq C-1`, or
          :math:`(N, d_1, d_2, ..., d_K)` with :math:`K \geq 1` in the case of
          K-dimensional loss.
        - Output: scalar.
          If :attr:`reduction` is ``'none'``, then the same size as the target: :math:`(N)`, or
          :math:`(N, d_1, d_2, ..., d_K)` with :math:`K \geq 1` in the case
          of K-dimensional loss.

    """

    def __init__(self, misclassified_threshold, rejection=False, weight=None, size_average=None, ignore_index=-100,
                 reduce=None, reduction='mean'):
        super(SoftML, self).__init__(size_average=size_average)
        self.nll_loss = nn.NLLLoss(weight=weight, size_average=size_average, ignore_index=ignore_index, reduce=reduce,
                                   reduction=reduction)
        self.misclassified_threshold = misclassified_threshold
        self.rejection = rejection
        # todo: add rejection

    def forward(self, x, target):
        exp_max_value = 10
        exp_min_value = -10

        # calculate likelihood matrix
        likelihood_matrix = self.misclassified_threshold + x
        likelihood_matrix[likelihood_matrix < exp_min_value] = exp_min_value
        max_indices = likelihood_matrix < exp_max_value
        likelihood_matrix[max_indices] = torch.log(1 + torch.exp(likelihood_matrix[max_indices]))
        likelihood_matrix = x + likelihood_matrix - torch.sum(likelihood_matrix, dim=1, keepdim=True)

        return self.nll_loss(likelihood_matrix, target)

# todo: add SoftDML
